#!/usr/bin/python3

import locale
import logging
from os import getcwd
from os.path import basename, splitext
import paramiko
from pprint import pprint
import sys
import yaml

locale.setlocale(locale.LC_ALL, '')

logfile     = getcwd() + "/" + splitext(basename(sys.argv[0]))[0] + ".log"

logging.basicConfig(filename = logfile,
                    level    = logging.DEBUG,
                    filemode = 'w',
                    encoding = 'utf-8', format = '%(levelname)s -- %(asctime)s - %(message)s')

log = logging.getLogger(__name__)

log.info("START")

try:
    configfile  = getcwd() + "/" + "config.yaml"
    log.info(f"CONFIG: {configfile}")
except:
    log.error(f"CONFIG: config.yaml nicht gefunden.")

with open(configfile, "rb") as config:
    data = yaml.safe_load(config)

host     = data["host"]
port     = data["port"]
username = data["username"]
password = data["password"]
src      = data["src"]
dst      = data["dst"]

for d in data:
    log.debug(f"{d}")
    log.debug(f"{data[d]}")

ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

try:
    ssh_client.connect(hostname = host,
                       port     = port,
                       username = username,
                       password = password)

    sftp = ssh_client.open_sftp()

    files = sftp.put(src, dst)

    log.debug(f"FILES: {type(files)}")

    sftp.close()
    ssh_client.close()
except Exception as e:
    log.error(f"Verbindung mit Server gescheitert: {e}")
    exit()

log.info("ENDE")

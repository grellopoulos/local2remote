Fr 15 Sep 2023 10:26:22 CEST
1694766382

Vorläufige Dokumentation für local2remote (wird noch
in Markdown umgewandelt...)

Das Skript kopiert eine lokal vorhandene Datei auf
einen entfernten Server. Es greift dabei auf eine
Konfigurationsdatei namens config.yaml zurück.
Je nachdem, ob es per crontab oder aber manuell
ausgeführt werden soll, ist config.yaml zu platzieren:

Der Einfachheit halber (quick and dirty) empfehle ich
bei Verwendung von crontab (cronie etc.) die oberste
Ebene des Benutzerverzeichnisses (/home/benutzer)
als Standort. Wird das Skript nur manuell
ausgeführt, kann config.yaml auch im selben Verzeichnis
stehen.

